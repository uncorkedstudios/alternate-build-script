//
//  UCAppDelegate.h
//  Alternate Build
//
//  Created by scott bates on 10/31/12.
//  Copyright (c) 2012 Uncorked Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UCViewController;

@interface UCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UCViewController *viewController;

@end
