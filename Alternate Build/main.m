//
//  main.m
//  Alternate Build
//
//  Created by scott bates on 10/31/12.
//  Copyright (c) 2012 Uncorked Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UCAppDelegate class]));
    }
}
